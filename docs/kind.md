# Using kind

https://kind.sigs.k8s.io/

Install kind

```bash

brew install kind

```

with kind and docker installed lets create a cluster

```bash

kind create cluster --name first-kind

```

It will take a bit for your cluster to start up.  The cluster details will be added your kubeconfig file.

```bash

#you should see your new cluster
kubectl config get-contexts

```

Check out the cluster running in docker.

```bash

docker ps

```

You will see a local docker container running with your cluster name.

Lets run a nginx pod on our kind cluster and try it out

```bash

kubectl run nginx --image nginx

#adjust the 8888 if the local port is in use
kubectl port-forward nginx 8888:80

```

You should now be able to browse to nginx on the forwarded port

http://localhost:8888/

