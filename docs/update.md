## Updating A Cluster

We can use the the declarative config defined by Custom Resources to manage the state of our kubernetes clusters.  For instance, we may want to add resources to the worker nodes in our cluster or update to a more recent version of Kubernetes.

We will do this by modifying the machine template our machine deployment uses.

```yaml

#lets save our current machine template to a file
kubectl get AWSMachineTemplate afc1-cluster-md-0 -o yaml > afc1-cluster-md-0.yaml

```

Tempalates are immutable, so we can not edit the values and re-apply.  We must create a new version of the template.

```yaml


kind: AWSMachineTemplate
metadata:
  #change the name
  name: afc1-cluster-md-0-v2
  namespace: default
  ownerReferences:
  - apiVersion: cluster.x-k8s.io/v1alpha3
    kind: Cluster
    name: afc1-cluster
    uid: 5397fe13-a457-4de1-8710-a65e7561b0fb
spec:
  template:
    spec:
      ami:
        id: ami-0abf74bc845d64fea
      iamInstanceProfile: nodes.tkg.cloud.vmware.com
      #change the instance type from large to xlarge
      instanceType: m5.xlarge
      rootVolume:
        size: 80
      sshKeyName: ubu2


```

Now we have updated the instance type and changed the template name.  Lets apply our new machine template into the cluster.

```bash

#file name based on the one we saved above
kubectl apply -f afc1-cluster-md-0.yaml

```

Now we have a new template, we need to edit the machine deployment to use it.


```bash

kubectl edit  machinedeployment afc1-cluster-md-0

```

This will open the machine deployment for editing.  Find the infrastructureRef, it will have a reference to the name of the machine template.  We need to update the reference to match our new machine template name, we added `-v2` onto the end of the name in this case.

```yaml

   infrastructureRef:
        apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
        kind: AWSMachineTemplate
        #make this match the updated template we just defined
        name: afc1-cluster-md-0-v2
      version: v1.18.8+vmware.1

```

We can now hop in the AWS Console and we will see the cluster going through a rolling update and replacing our large nodes with xlarge nodes.  This update process will migrate our workloads by draining and cordoning our existing clusters before  replacing it.  Work is migrated to another cluster ensuring that our services stay available.

### what about upgrading Kubernetes?

We can follow a similar pattern to upgrade Kubernetes.  Kubernetes version is managed through the use of machine images.  In AWS these are called Amazon Machine Images (AMI).  AMI's use a reference ID to locate and use them.  The Cluster API project publishes AMI's in AWS for specific versions of Kubernetes.

For a version update, we start with the control plane and then move to the worker nodes.  

```bash

kubectl get kubeadmcontrolplane -o yaml


```

By inspecting the yaml, you will notice the controlplane configuration has a reference to the infrastructure template, similar to the machinedeployment from earlier.

```yaml

  infrastructureTemplate:
      apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
      kind: AWSMachineTemplate
      name: afc1-cluster-control-plane
      namespace: default

```

Let's save this machinetemplate to a file, like last time.

```bash

kubectl get AWSMachineTemplate afc1-cluster-control-plane -o yaml > afc1-cluster-control-plane.yaml

```

If we open our file, we will notice that there is a reference to an AMI.  We will want to update this AMI reference to update the Kubernetes version of our control plane.

```yaml

#change the name of the template
name: afc1-cluster-control-plane-v2

#update the AMI reference
template:
    spec:
      ami:
        id: ami-0abf74bc845d64fea

```

Now that we have updated the AMI ID and name, lets reapply the tempalte.


```bash

kubectl apply -f afc1-cluster-control-plane.yaml

```

Now we need to update our kubeadmcontrolplane resource to reference our new tempalte.

```bash

kubectl edit kubeadmcontrolplane afc1-cluster-control-plane 

```

Update the infrastructureTemplate to reference the new tempalte.

```yaml

  infrastructureTemplate:
      apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
      kind: AWSMachineTemplate
      name: afc1-cluster-control-plane
      namespace: default

```

If we look in our AWS console we will see a new instance creating.  We can repeat the same steps for our worker nodes by updating the AMI reference in the AWSMachineTemplate referenced by the cluster's machinedeployment.