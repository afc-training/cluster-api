## Cluster API Custom Resources

cluster.cluster.x-k8s.io

```yaml

  apiVersion: cluster.x-k8s.io/v1alpha3
  kind: Cluster
  metadata:
    name: afc1-cluster
    namespace: default
  spec:
    clusterNetwork:
      pods:
        cidrBlocks:
        - 100.96.0.0/11
      services:
        cidrBlocks:
        - 100.64.0.0/13
    controlPlaneEndpoint:
      host: afc1-cluster-apiserver-1553412645.us-east-1.elb.amazonaws.com
      port: 6443
    controlPlaneRef:
      apiVersion: controlplane.cluster.x-k8s.io/v1alpha3
      kind: KubeadmControlPlane
      name: afc1-cluster-control-plane
      namespace: default
    infrastructureRef:
      apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
      kind: AWSCluster
      name: afc1-cluster
      namespace: default

```

kubeadmcontrolplane.controlplane.cluster.x-k8s.io

```yaml

apiVersion: controlplane.cluster.x-k8s.io/v1alpha3
kind: KubeadmControlPlane
metadata:
  name: afc1-cluster-control-plane
  namespace: default
spec:
  infrastructureTemplate:
    apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
    kind: AWSMachineTemplate
    name: afc1-cluster-control-plane
  kubeadmConfigSpec:
    clusterConfiguration:
      apiServer:
        extraArgs:
          cloud-provider: aws
          tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_256_GCM_SHA384
        timeoutForControlPlane: 8m0s
      controllerManager:
        extraArgs:
          cloud-provider: aws
          tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
      dns:
        imageRepository: registry.tkg.vmware.run
        imageTag: v1.7.0_vmware.3
        type: CoreDNS
      etcd:
        local:
          dataDir: /var/lib/etcd
          extraArgs:
            cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
          imageRepository: registry.tkg.vmware.run
          imageTag: v3.4.13_vmware.2
      imageRepository: registry.tkg.vmware.run
      scheduler:
        extraArgs:
          tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
    initConfiguration:
      nodeRegistration:
        kubeletExtraArgs:
          cloud-provider: aws
          tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
        name: '{{ ds.meta_data.local_hostname }}'
    joinConfiguration:
      nodeRegistration:
        kubeletExtraArgs:
          cloud-provider: aws
          tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
        name: '{{ ds.meta_data.local_hostname }}'
    useExperimentalRetryJoin: true
  replicas: 1
  version: v1.19.1+vmware.2

```

kubeadmconfigtemplate.bootstrap.cluster.x-k8s.io

```yaml

apiVersion: bootstrap.cluster.x-k8s.io/v1alpha3
kind: KubeadmConfigTemplate
metadata:
  name: afc1-cluster-md-0
  namespace: default
spec:
  template:
    spec:
      joinConfiguration:
        nodeRegistration:
          kubeletExtraArgs:
            cloud-provider: aws
            tls-cipher-suites: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
          name: '{{ ds.meta_data.local_hostname }}'
      useExperimentalRetryJoin: true

```

machinedeployment.cluster.x-k8s.io

```yaml

apiVersion: cluster.x-k8s.io/v1alpha3
kind: MachineDeployment
metadata:
  name: afc1-cluster-md-0
  namespace: default
spec:
  clusterName: afc1-cluster
  replicas: 1
  selector:
    matchLabels: null
  template:
    metadata:
      labels:
        node-pool: afc1-cluster-worker-pool
    spec:
      bootstrap:
        configRef:
          apiVersion: bootstrap.cluster.x-k8s.io/v1alpha3
          kind: KubeadmConfigTemplate
          name: afc1-cluster-md-0
      clusterName: afc1-cluster
      failureDomain: us-east-1a
      infrastructureRef:
        apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
        kind: AWSMachineTemplate
        name: afc1-cluster-md-0
      version: v1.19.1+vmware.2

```


## AWS Cluster API Provider Resources

awscluster.infrastructure.cluster.x-k8s.io

```yaml

apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
kind: AWSCluster
metadata:
  name: afc1-cluster
  namespace: default
spec:
  bastion:
    enabled: true
  networkSpec:
    cni:
      cniIngressRules:
      - description: antrea1
        fromPort: 10349
        protocol: tcp
        toPort: 10349
      - description: genev
        fromPort: 6081
        protocol: udp
        toPort: 6081
    subnets:
    - availabilityZone: us-east-1a
      cidrBlock: 10.0.2.0/24
      id: ""
      isPublic: true
    - availabilityZone: us-east-1a
      cidrBlock: 10.0.0.0/24
      id: ""
    vpc:
      cidrBlock: 10.0.0.0/16
      id: ""
  region: us-east-1
  sshKeyName: ubu2

```

awsmachinetemplate.infrastructure.cluster.x-k8s.io

```yaml

apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
kind: AWSMachineTemplate
metadata:
  name: afc1-cluster-control-plane
  namespace: default
spec:
  template:
    spec:
      ami:
        id: ami-0647eed5634b73cd5
      iamInstanceProfile: control-plane.tkg.cloud.vmware.com
      instanceType: m5.large
      rootVolume:
        size: 80
      sshKeyName: ubu2

```

awsmachines.infrastructure.cluster.x-k8s.io     

```yaml

apiVersion: infrastructure.cluster.x-k8s.io/v1alpha3
  kind: AWSMachine
  metadata:
    annotations:
      cluster-api-provider-aws: "true"
      cluster.x-k8s.io/cloned-from-groupkind: AWSMachineTemplate.infrastructure.cluster.x-k8s.io
      cluster.x-k8s.io/cloned-from-name: afc1-cluster-md-0
 name: afc1-cluster-md-0-5rg42
    namespace: default
    ownerReferences:
    - apiVersion: cluster.x-k8s.io/v1alpha3
      blockOwnerDeletion: true
      controller: true
      kind: Machine
      name: afc1-cluster-md-0-5f849c9dc5-hc5zp
      uid: f7ad2288-afc6-4985-9780-5bfc10a034e3
  spec:
    ami:
      id: ami-0647eed5634b73cd5
    cloudInit: {}
    iamInstanceProfile: nodes.tkg.cloud.vmware.com
    instanceType: m5.large
    providerID: aws:///us-east-1a/i-0a118d855757cf046
    rootVolume:
      size: 80
    sshKeyName: ubu2

```