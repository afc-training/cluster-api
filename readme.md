# Working with Cluster API


### Cluster API Management Cluster
Cluster API uses a managment cluster with Custom Resources to manage the state of many workload clusters.
  
[Management Cluster](docs/mgmt.md)

### kind clusters
Cluster API often uses a kind cluster to initialize a management cluster in a given infrastructure provider.  kind stands for Kubernetes IN Docker.  It allows you to stand up a Kubernetes within a docker container for testing or you may find a creative use for this within a CI Pipeline or other processes (like Cluster API does).

[Kind setup](docs/kind.md)

### Creating a Workload Cluster 
The clusters folder contains a predefined set of AWS cluster definitions.  We can apply these to our mgmt cluster and the Cluster API AWS Controller will begin to create the AWS constructs defined in the cluster definition.

### Anatomy of a Cluster on AWS
Lets walk through the AWS Clusters we just created and take a deeper look at the infrastructure components used to instantiate our cluster.

### Multi-AZ Clusters
For critcal workloads we want to ensure that our clusters are always available, even if there is an outage in a specific part of AWS.  Cluster API has pre-defined high availability deployment patterns that deploy a cluster accross multiple availability zones providing added insurance that an isolated outage will not prevent service availability.

### Updating a Workload Cluster
Cluster API provides cluster life-cycle management for us.  This includes making updates to kubernetes, scaling clusters, and making common configurations for things like CA Certs.

[Updating Clusters](docs/update.md)


### Managing Clusters with GitOps
One of the most powerful capabilites that Cluster API enables is managing our kubernetes cluster infrastructure using GitOps.  We have explored using ytt and kapp controller to manage a GitOps workflow, lets see if we can use it to manage our actual infrastructure.
